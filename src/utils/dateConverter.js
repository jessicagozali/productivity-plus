export const displayPrettyDate = (date) => {
  // Days
  const nameOfDays = [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat'
  ];

  // Months
  const nameOfMonths = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];

  const day = nameOfDays[date.getDay()];
  const dateNum = date.getDate();
  const month = nameOfMonths[date.getMonth()];
  const year = date.getFullYear();

  return (`${day}, ${dateNum} ${month} ${year}`);
}

