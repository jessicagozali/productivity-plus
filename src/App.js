import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { createTheme, ThemeProvider } from '@mui/material/styles';

import Header from './components/Header';
import Home from './views/Home';

// For website theme
const theme = createTheme({
  palette: {
    primary: {
    main: '#8d50c9',
    light: '#c07ffd',
    dark: '#5b2297',
    },
  },
  typography: {
    color: '#353A4A',
  },
});

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Header />
      <BrowserRouter>
        <Routes>
          <Route exact path='/' element={<Home />} />
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  )
}

export default App
