import Container from '@mui/material/Container';
import TodayDate from '../components/ToDo/TodayDate';
import Widgets from '../components/Widgets/Widgets';

const Home = () => {
  return (
    <div className="page-view home-page">
      <Container>
        <TodayDate />
        <Widgets />
      </Container>
    </div>
  )
}

export default Home
