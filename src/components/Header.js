import { styled } from '@mui/material/styles';
import {
  Box,
  AppBar,
  Toolbar,
  IconButton,
  Badge,
  MenuItem,
  Typography
} from '@mui/material';
import AddTaskIcon from '@mui/icons-material/AddTask';
import NotificationsIcon from '@mui/icons-material/Notifications';

import RightDrawer from './RightDrawer';

const StyledToolbar = styled(Toolbar)(({ theme }) => ({
  alignItems: 'center',
  paddingTop: theme.spacing(.5),
  paddingBottom: theme.spacing(.5),
  // Override media queries injected by theme.mixins.toolbar
  '@media all': {
    minHeight: 64,
  },
}));

const Header = () => {
  return (
    <header>
      <Box sx={{ flexGrow: 1 }} className="nav-menu">
        <AppBar position="static">
          <StyledToolbar>
            <div>
              <AddTaskIcon
                className="logo-icon"
                onClick={e => window.location.href='/'}
              />
              <Typography
                className="logo-text"
                variant="h1"
                noWrap
                component="div"
                onClick={e => window.location.href='/'}
              >
                Fancy Dailies
              </Typography>
            </div>
            <div>
              <MenuItem>
                <IconButton
                  size="large"
                  edge="end"
                  aria-label="show 17 new notifications"
                  color="inherit"
                >
                  <Badge
                    badgeContent={17}
                    color="error"
                  >
                    <NotificationsIcon />
                  </Badge>
                </IconButton>
              </MenuItem>
              <RightDrawer />
            </div>
          </StyledToolbar>
        </AppBar>
      </Box>
    </header>
  );
}

export default Header;
