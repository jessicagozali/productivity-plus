import Grid from '@mui/material/Grid';

import Widget from './Widget';

const widgets = ["Weather", "TripView", "Outfit"];

const Widgets = () => {
  return (
    <Grid container justifyContent="center" spacing={2}>
      {widgets.map((widget, i) => (
        <Grid item key={i} xs={4}>
          <Widget widget={widget} />
        </Grid>
      ))}
    </Grid>
  )
}

export default Widgets;
