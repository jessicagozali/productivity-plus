import {
  Card,
  CardContent
} from '@mui/material';

import Weather from './Weather';
import TripView from './TripView';
import Outfit from './Outfit';

const Widget = ({ widget }) => {
  const getWidget = (widget) => {
    switch (widget) {
      case 'Weather':
        return (<Weather />);
      case 'TripView':
        return (<TripView />);
      case 'Outfit':
        return (<Outfit />);
      default:
        break;
    }
  };

  return (
    <Card variant="outlined">
      <CardContent>
        {getWidget(widget)}
      </CardContent>
    </Card>
  )
}

export default Widget;
