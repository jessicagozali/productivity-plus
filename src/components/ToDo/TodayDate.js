import Typography from '@mui/material/Typography';
import { displayPrettyDate } from '../../utils/dateConverter';

const getTodayDate = () => {
  return displayPrettyDate(new Date());
};

const TodayDate = () => {
  return (
    <>
      <Typography className="upper-text">
        Today is
      </Typography>
      <Typography className="date-display">
        {getTodayDate()}
      </Typography>
    </>
  )
}

export default TodayDate;
